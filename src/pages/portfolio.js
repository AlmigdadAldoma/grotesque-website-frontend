import React, {useState} from "react";
import { Link,graphql, useStaticQuery } from "gatsby";
import Layout from "../components/layout";
import TransitionLink  from "gatsby-plugin-transition-link";
import AniLink from "gatsby-plugin-transition-link/AniLink"
import "../assets/css/main.css";
import Img from "gatsby-image";
import { motion, AnimatePresence } from "framer-motion"
import Nav from "../components/nav";
import { TransitionPortal } from "gatsby-plugin-transition-link";


const variants = {
  Hovered: { opacity: 1, width:"90%", },
  notHoverd: { opacity: 0.5,display: "none"},
}

const PortfolioPage = () => {
const [isOpen, setIsOpen] = useState(false);

const [isHovered, setisHovered] = useState(false);

  const query = graphql`
query {
    allStrapiProjects {
        edges {
          node {
            Title
            Slug
            videoLink
            strapiId
            Description
            cover {
              publicURL
                childImageSharp {
                  fluid {
                  ...GatsbyImageSharpFluid
      
                  } 
                }
              }
          }
        }
      }

  }
`;
  const data = useStaticQuery(query);
  return (
    
    <div>
      <AnimatePresence>
      <TransitionPortal>
    <Nav isOpen={isOpen} setIsOpen={setIsOpen} />
      </TransitionPortal>
      <div className="portfolio-section">
        <div className="portfolio-list">
          {data.allStrapiProjects.edges.map((project, i) => {
            return (
              <div className="project-display" key={project.node.strapiId}>
                <motion.span onMouseEnter={() => setisHovered(i)} onMouseLeave={() => setisHovered(false)}>
                <Link to={`/portfolio/${project.node.Slug}`} className="project-title">{project.node.Title}</Link>
                </motion.span>
                <div className="project-thumbnail" >
                <motion.img  transition={{ duration: .5, ease:"easeInOut" }} animate={i === isHovered ? "Hovered" : "notHovered"} variants={variants} src={project.node.cover.publicURL} alt="..." />
                </div>
              </div>
            );
          })}
        </div>
      <div className="film-prod-title">
          <motion.a to="#">
            <h2>Film & Production
            </h2>
            </motion.a>
        </div>
      </div>
      </AnimatePresence>
    </div>
  );
};





export default PortfolioPage;