import React, {useState,useLayoutEffect, useRef} from "react";
import { Link,graphql, useStaticQuery } from "gatsby";
import Layout from "../components/layout";
import "../assets/css/main.css";
import Img from "gatsby-image";
import { useViewportScroll, motion, useTransform } from "framer-motion"
import Nav from "../components/nav";
import { TransitionPortal } from "gatsby-plugin-transition-link";


const PhotographyPage = () => {

const [isZoom, setIsZoom] = useState(false);
const [isPicZoom, setisPicZoom] = useState(false);

const [isOpen, setIsOpen] = useState(false);

const modalVariants = {
  open: { display: "block", opacity:1, },
  closed: { opacity: 1, display: "none"},
}
const zoomVariants = {
  zoomed: { 
    opacity:[0,1],
    zIndex:30, 
    pointerEvents:"none",
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    maxWidth: "100%",
    maxHeight: "80%",
    transition: {
      duration: .2,
      type:'tween',
      opacity: { ease: 'easeOut', duration: .5 }
    }
    
   },
  notZoomed: { 
    zIndex:"none",
    opacity:[0,0,1], 
    transition: {
    duration: .2,
    type:'tween',
    opacity: { ease: 'easeOut', duration: .5 }
  }
},
}

const zoomVariantsContainer = {
  zoomed: { 
    zIndex:30, 
    pointerEvents:"none",
   },
  notZoomed: { zIndex:"none" },
}

const closeBtnVariants = {
  showBtn: { 
    display: "block" },
  hideBtn: {  display: "none" },
}

const [elementTop, setElementTop] = useState(0);
const { scrollY } = useViewportScroll();
const y1 = useTransform(scrollY, [0, elementTop + elementTop], [0, -100]);
const ref = useRef(null);
useLayoutEffect(() => {
    const element = ref.current;
    setElementTop(element.offsetTop);
  }, [ref]);

  const query = graphql`
query {
    allStrapiPhotographies {
        edges {
          node {
            id
            photos {
              publicURL
              childrenImageSharp {
                fluid(maxWidth: 600) {
                    ...GatsbyImageSharpFluid
                  }
              }
            }
          }
        }
      }

  }
`;
  
  const data = useStaticQuery(query);
  const toggleSwitch = () => { setIsZoom(!isZoom); };

  const toggleClose = () => { setisPicZoom(false); setIsZoom(!isZoom); };
//   console.log(data.allStrapiPhotographies.nodes);
  return (
    
    <div>
     <TransitionPortal >
    <Nav isOpen={isOpen} setIsOpen={setIsOpen} />
      </TransitionPortal>
      <div className="photography-section">
              <div ref={ref} className="photo-grid">
          {data.allStrapiPhotographies.edges.map((photo, i) => {
            return (
              <div>
              <motion.div variants={zoomVariantsContainer} animate={i+1 === isPicZoom ? "zoomed" : "notZoomed"}  className="photo-container"  onClick={toggleSwitch} data-isZoom={isZoom} key={i+1} data-key={i+1}>
                <motion.img variants={zoomVariants} onClick={() => setisPicZoom(i+1) } data-isPicZoom={isPicZoom} data-key={i+1}   key={i+1} src={photo.node.photos.publicURL} />
                <a href={photo.node.photos.publicURL}>
                <motion.img className="mobile-fix"  key={i+1} src={photo.node.photos.publicURL} />
                </a>
              </motion.div>
                          </div>
                  );
                })}              
                <motion.a className="close-btn"  variants={closeBtnVariants} animate={isZoom ? "showBtn" : "hideBtn"} onClick={toggleClose} href="#" >closed</motion.a>
              </div>
      </div>
      
                <motion.div animate={isZoom ? "open" : "closed"} variants={modalVariants} className="zoom-modal"></motion.div>
      
    </div>
  );
};





export default PhotographyPage;