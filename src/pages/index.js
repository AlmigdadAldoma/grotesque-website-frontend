import React, { useState, useRef } from "react";
import { graphql, useStaticQuery } from "gatsby";
import Layout from "../components/layout";
import Nav from "../components/nav";
import "../assets/css/style.scss";
import HeroSection from "../components/heroSection";
import AboutSection from "../components/aboutSection";
import WorkSection from "../components/workSection";
import ServicesSection from "../components/servicesSection";
import ContactSection from "../components/contactSection";
import { TransitionPortal } from "gatsby-plugin-transition-link";







const IndexPage = () => {
  const data = useStaticQuery(query);
  const [isOpen, setIsOpen] = useState(false);
  const [isVidHovered, setVidHovered] = useState(false);
  const [isPicHovered, setPicHovered] = useState(false);
  const [elementTop, setElementTop] = useState(0);
  
  return (
    <Layout>
      <TransitionPortal>
    <Nav isOpen={isOpen} setIsOpen={setIsOpen} />
      </TransitionPortal>
    <HeroSection ></HeroSection>
    <AboutSection elementTop={elementTop} setElementTop={setElementTop}></AboutSection>
    <WorkSection isVidHovered={isVidHovered} setVidHovered={setVidHovered} isPicHovered={isPicHovered} setPicHovered={setPicHovered}></WorkSection>
    <ServicesSection></ServicesSection>
    <ContactSection></ContactSection>
  </Layout>
  );
};

const query = graphql`
  query {
    strapiHomepage {
      hero {
        title
      }
      seo {
        metaTitle
        metaDescription
        shareImage {
          publicURL
        }
      }
    }
  }
`;

export default IndexPage;