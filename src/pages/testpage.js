import React, { useState } from "react";

import Layout from "../components/layout";
import Nav from "../components/nav";
import "../assets/css/main.css";
import HeroSection from "../components/heroSection";


const TestPage = () => {

  const [isOpen, setIsOpen] = useState(false);

  return (
    <Layout >
      <Nav isOpen={isOpen} setIsOpen={setIsOpen} />
      <HeroSection ></HeroSection>
    </Layout>
  );
};



export default TestPage;