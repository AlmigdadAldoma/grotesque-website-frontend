import React, {useState} from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";
import Nav from "../components/nav";
import { motion, AnimatePresence } from "framer-motion"


export const query = graphql`
  query ProjectsQuery($Slug: String!) {
    strapiProjects(Slug: {eq: $Slug}) {
    Description
    Slug
    Title
    videoLink
    id
    cover {
        publicURL
      }
  }
  }
`;




const Project = ({ data }) => {
  const modalVariants = {
    open: { display: "grid", opacity:1, },
    closed: { opacity: 0, display: "none", transition:{duration:.5,ease:"easeInOut"}},
  }
  
  const [isModalOpen, setIsModalOpen] = useState(false);
  
  const [isOpen, setIsOpen] = useState(false);
  const project = data.strapiProjects;
  const seo = {
    metaTitle: project.title,
    metaDescription: project.description,
    shareImage: project.cover.publicURL,
    project: true,
  };
  
// const rawVideoLink = project.VideoLink.replace(/['"]+/g, '');
// console.log(JSON.parse(project.oembed));
// console.log(project.oembed);
// try {
//    JSON.parse(project.oembed);
// } catch(e) {console.log(e)}

  return (
    <Layout seo={seo}>
    <AnimatePresence>
      <motion.div>
    <Nav isOpen={isOpen} setIsOpen={setIsOpen} />
      </motion.div>
      <div key={project.id} className="project-container">
        <div className="project">
          {/* <div className="project-title">
          <h2>{project.Title}</h2>
          </div> */}
          <div className="project-preview" style={{ position:"relative"}}>
          <motion.div className="project-thumbnail" onClick={() => setIsModalOpen(true) } >
          <motion.img className="Thumb-play"   style={{width:"100%",zIndex:30}} src={project.cover.publicURL} alt="..." />
          </motion.div>          
          </div>
          <motion.div transition={{duration:.5, ease:"easeInOut"}} initial={{ opacity:0,y:-10 }} animate={{opacity:1,y:0}} className="project-description">{project.Description}</motion.div> 
      </div>
      </div>
      <motion.div key={project.id+1} className="videoModal"  transition={{duration:.5, ease:"easeIn"}} variants={modalVariants} animate={isModalOpen ? "open" : "closed"} >
      <div dangerouslySetInnerHTML={{ __html: project.videoLink }} />
      <motion.a className="close-btn"  onClick={() => setIsModalOpen(false)} href="#" >closed</motion.a>
      </motion.div>
      </AnimatePresence>
    </Layout>
  );
};

export default Project;