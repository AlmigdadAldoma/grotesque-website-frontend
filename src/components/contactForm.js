import React from "react";

export default class contactForm extends React.Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state = {
      status: ""
    };
  }

  render() {
    const { status } = this.state;
    return (
      <form
        onSubmit={this.submitForm}
        action="https://formspree.io/f/xqkgodzv"
        method="POST"
      >
        <div className="form-groups">
                        <div className="form-group">
                            <input type="text" className="form-control" name="name" placeholder="Name" />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" name="email" placeholder="E-mail" />
                        </div>
                        <div className="form-group">
                            <textarea className="form-control" name="body" placeholder="Message" cols="30" rows="10"></textarea>
                        </div>
                                {/* <button type="submit" className="btn-send pull-right">Send</button> */}
        {status === "SUCCESS" ? <p>Thanks you, we will contact you soon!</p> :<button type="submit" className="btn-send pull-right">SEND</button> }
        {status === "ERROR" && <p>Ooops! There was an error.</p>}
        </div>
      </form>
    );
  }

  submitForm(ev) {
    ev.preventDefault();
    const form = ev.target;
    const data = new FormData(form);
    const xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        form.reset();
        this.setState({ status: "SUCCESS" });
      } else {
        this.setState({ status: "ERROR" });
      }
    };
    xhr.send(data);
  }
}