import React, { useState, useRef } from "react";
import TransitionLink  from "gatsby-plugin-transition-link";
import AniLink from "gatsby-plugin-transition-link/AniLink"

import { graphql, Link } from "gatsby";
import Photo from "../images/photo.jpg";
import FilmProd from "../images/film&prod.mp4";
import { motion } from "framer-motion"
import PortfolioPage from "../pages/portfolio";



const variantsPicture = {
  Pichoverd: {  opacity:1, scale:1.2 },
  PicnotHoverd: { opacity: 0, scale:1 },
}


const variantsVideo = {
  Vidhoverd: {  opacity:1, scale:1.2 },
  VidnotHoverd: { opacity: 0, scale:1 },
}

const WorkSection = ({flmRef,isVidHovered, setVidHovered,isPicHovered, setPicHovered}) => {
  
  return (
    <div className="work-section">
       <div className="film-prod-section">
      <AniLink paintDrip hex="#121212"  to="/portfolio/">
        <motion.h2
        onMouseEnter={() => setVidHovered(true)}
        onMouseLeave={() => setVidHovered(false)}
        >Film & Production</motion.h2>
        </AniLink >
        <div className="overlay"></div>
        <motion.video transition={{
          opacity: { duration: .2  },
          scale: {  duration: 2 , ease:[.03,.01,0,.99] },
          }} 
          variants={variantsVideo} animate={  isVidHovered ? "Vidhoverd" : "VidnotHoverd" } loop autostart autoPlay muted src={FilmProd} type="video/mp4" />
       </div>
       <div className="photo-section">
       <AniLink paintDrip hex="#121212" to="/photography/">
       <motion.h2
       onMouseEnter={() => setPicHovered(true)}
       onMouseLeave={() => setPicHovered(false)}
       >Photography</motion.h2>
       </AniLink>
       <div className="overlay"></div>
       <motion.img src={Photo} transition={{
          opacity: { duration: .2  },
          scale: {  duration: 5 , ease:[.03,.01,0,.99] },
          }} 
           variants={variantsPicture} animate={  isPicHovered ? "Pichoverd" : "PicnotHoverd" } />
       </div>
    </div>
  );
};





export default WorkSection;