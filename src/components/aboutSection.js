import React from "react";
import { useLayoutEffect, useRef } from "react";
import { graphql, useStaticQuery, Link } from "gatsby";
import Layout from "../components/layout";
import Img from "gatsby-image";
import { useViewportScroll, motion, useTransform } from "framer-motion";
import AboutImage1 from "../images/about-image.jpg";
import AboutImage2 from "../images/about-image2.jpg";
import AboutImage3 from "../images/about-image3.jpg";

  

const AboutSection = ({elementTop,setElementTop}) => {
 
  const { scrollY } = useViewportScroll();
  const ref = useRef(null);

  const y1 = useTransform(scrollY, [0, elementTop + elementTop], [0, -100]);
  const x1 = useTransform(scrollY, [0, elementTop + elementTop], [0, 100]);
  const y2 = useTransform(scrollY, [0, elementTop + elementTop], [0, 200]);
  const y3 = useTransform(scrollY, [0, elementTop + elementTop], [0, -200]);
  useLayoutEffect(() => {
    const element = ref.current;
    setElementTop(element.offsetTop);
  }, [ref]);
  //console.log("current is"+ref.current.offsetTop);

  return (
    <div>
        <div className="about-container">
        <div className="about-text">
            <h2>About Grotesque</h2>
            <div className="about-text-inner">
            We provide fresh ideas and new concepts that cater for your targeted audience. We take on full productions from the Concept development, Full scale production to Post production and audio design. 
            <br/><br/>
            We  also provide Digital Content & Filmmaking workshops in Sudan, and aims to blur the lines between work and play, in the creative industry.
            </div>
        </div>
        <div ref={ref} className="about-images">
        <motion.img style={{y:y1,x:x1}} src={AboutImage2} />
        <motion.img style={{y:y2}} src={AboutImage3} />
        <motion.img style={{y:y3}} src={AboutImage1} />
        <motion.img  src={AboutImage1} />
        
        </div>
        </div>
    </div>
  );
};





export default AboutSection;