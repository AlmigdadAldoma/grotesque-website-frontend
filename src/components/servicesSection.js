import React from "react";
import { graphql, useStaticQuery, Link } from "gatsby";
import Img from "gatsby-image";
import BgVideo from "../images/video.mp4"

const ServicesSection = () => {
  return (
    <div>
     <div className="services-container">
        {/* <h2>Services</h2> */}
        <div className="services-inner">
            <div className="service">
                <h2>Creative Concept Development</h2>
                <p>Our team is constantly creating Fresh & relevant concepts which presents your brand in the most unique way you can imagine.</p>
            </div>
            <div className="service">
                <h2>Production</h2>
                <p>our team takes on full scale production. from lighting, art direction, prop design to cinematography and sound.</p>
            </div>
            <div className="service">
                <h2>Script Writing</h2>
                <p>whether it's a tvc, a documentary, a short film, a social media campaign. we constantly come up with fresh stories to narrate your project, from a new perspective.</p>
            </div>
            <div className="service">
                <h2>Post-Production</h2>
                <p>our creative editors work with what they have, and sometimes create new content, to complete each element of the post production process in the most innovative way possible. complimenting the content with color grading, vfx and motion graphics skills.</p>
            </div>
            
        </div>
     </div>
    </div>
  );
};





export default ServicesSection;