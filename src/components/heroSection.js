import React  from "react";
import { graphql, useStaticQuery, Link } from "gatsby";
import Img from "gatsby-image";
import logo from "../images/logo.png"
import BgVideo from "../images/video.mp4"
import { useViewportScroll, motion, useTransform,useMotionValue } from "framer-motion";



const HeroSection = () => {

  const { scrollYProgress } = useViewportScroll()
  const opacity = useTransform(scrollYProgress, [0, 0.08, 1], [1, 0, 0])

 
  return (
    <div>
      <div className="hero-container">
        <div className="hero-text">
          <motion.div className="hero-paragraph" opacity={opacity}>
          <img className="logo-image" src={logo} />
          </motion.div>
          {/* <div className="hero-cta">
            <Link to="#">See Works</Link>
            <Link to="#">Contact</Link>
          </div> */}
          <div className="scroll-indicator">
            <div className="scroll-vertical-line">
              <motion.div animate={{ height: "100%" }} transition={{ ease: "easeOut", duration: 2, repeat: Infinity }} className="vertical-line"></motion.div>
            </div>
          <span>SCROLL</span>
          </div>
        </div>
        <div className="hero-video">
          <div className="video-overlay"></div>
          <video loop autostart="autostart" autoPlay muted src={BgVideo} type="video/mp4" />
        </div>

      </div>
    </div>
  );
};





export default HeroSection;