import React, { useState } from "react";
import { Link, StaticQuery, graphql } from "gatsby";
import AniLink from "gatsby-plugin-transition-link/AniLink"
import logo from "../images/logo.png"
import { Slant as Hamburger } from 'hamburger-react'
import { motion } from "framer-motion"




const variantsParent = {
  open: { opacity: 1, y: 0 },
  closed: { opacity: 0, y: "-100%" },
}

const NavItemParent = {
  open: { transition: {
    delayChildren: 0.5,
    staggerChildren: 0.2
  } },
  closed: { transition: {
    staggerChildren: 0
  } },
}
const NavItem = {
  open: { opacity: 1 },
  closed: {  opacity: 0},
}




const Nav = ({isOpen, setIsOpen}) => (
  <StaticQuery
    query={graphql`
      query {
        strapiGlobal {
          siteName
        }
        allStrapiCategory {
          edges {
            node {
              slug
              name
            }
          }
        }
      }
    `}
    render={(data) => (
      
      <motion.div  initial={{opacity:0}} animate={{opacity:1}} transition={{duration:.5, ease:"easeIn"}}>
        <motion.div initial={{position:"fixed"}} className="nav-container">
          <div className="logo-container">
          <AniLink paintDrip hex="#121212"  to="/">
          <img className="logo-image" src={logo} />
          </AniLink >
          </div>
          <div className="nav-toggle">
            <Hamburger toggled={isOpen} toggle={setIsOpen} />
          </div>
          <motion.div
          animate={isOpen ? "open" : "closed"}
          variants={variantsParent}
          transition={{ type: 'tween', duration:.5 }}
          initial={false}
          className="nav-links">
          <motion.nav>
                  <motion.div 
                  variants={NavItemParent} 
                  animate={isOpen ? "open" : "closed"}
                  className="right-links">
                    <motion.h2 variants={NavItem}>About</motion.h2>
                    <AniLink paintDrip hex="#121212"  to="/"><motion.div variants={NavItem}>Home</motion.div></AniLink >
                    <AniLink paintDrip hex="#121212"  to="#"> <motion.div variants={NavItem}>Contact</motion.div></AniLink >
                  </motion.div>
                  <motion.div 
                  variants={NavItemParent} 
                  animate={isOpen ? "open" : "closed"}
                  className="left-links">
                    <motion.h2 variants={NavItem}>Works</motion.h2>
                    <AniLink paintDrip hex="#121212"  to="/portfolio/"><motion.div variants={NavItem}>Film & Production</motion.div></AniLink >
                    <AniLink paintDrip hex="#121212"  to="/photography/"><motion.div variants={NavItem}>Photography</motion.div></AniLink >
                  </motion.div>
            </motion.nav>
          </motion.div>
        </motion.div>
        
      </motion.div>
    )}
  />
);

export default Nav;
