import React from "react";
import { graphql, useStaticQuery, Link } from "gatsby";
import Img from "gatsby-image";
import BgVideo from "../images/video.mp4"
import ContactForm from "../components/contactForm";


const ContactSection = () => {
    const data = useStaticQuery(graphql`
    query {
      allClinetsJson {
        nodes {
          id
          name
          src {
            childImageSharp {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  `)


  return (
    <div>
       
     <div className="contact-section">
         <div className="contact-form">
             <div className="cta-text">
                 <h2>let's have a chat</h2>
                 <p>lorem ipsum dolor sit amet, consectetur adipiscing elit. nulla semper, nunc ut tempus pretium,</p>
             </div>
             <div className="cta-form">
                      {/* <form action="https://formspree.io/f/xqkgodzv" method="POST">
                      <div className="form-groups">
                        <div className="form-group">
                            <input type="text" className="form-control" name="name" placeholder="Name" />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" name="email" placeholder="E-mail" />
                        </div>
                        <div className="form-group">
                            <textarea className="form-control" name="body" placeholder="Message" cols="30" rows="10"></textarea>
                        </div>
                                <button type="submit" className="btn-send pull-right">Send</button>
                    </div>
                      </form> */}
                      <ContactForm></ContactForm>

             </div>
         </div>
         <div className="clients">
                  {
                      data.allClinetsJson.nodes.map(client => (
                      <div key={client.id} className="client-wrapper">
                              { client.src &&
                                <div className="client-logo" style={{
                                    display: `inline`,
                                    marginBottom: `1.45rem`,
                                  }}>
                                  <Img style={{position: `relative`,filter: `grayscale(100%)`}} fluid={client.src.childImageSharp.fluid} />
                        </div>          
                              }
                              </div>
                             
                          
                      ))
                  }
             
         </div>
     </div>
    </div>
  );
};






export default ContactSection;