/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

// You can delete this file if you're not using it
// const addScript = url => {
//     const script = document.createElement("script")
//     script.src = url
//     script.async = true
//     document.body.appendChild(script)
//   }
  
//   export const onInitialClientRender  = () => {
//     window.onload = () => {
//       addScript("https://cdn.jsdelivr.net/npm/circletype@2.3.0/dist/circletype.min.js")
//     }
//   }