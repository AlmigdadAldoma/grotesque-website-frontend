/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
// exports.createPages = async ({ graphql, actions }) => {
//     const { createPage } = actions;
//     const result = await graphql(
//       `
//         {
//           articles: allStrapiArticle {
//             edges {
//               node {
//                 strapiId
//                 slug
//               }
//             }
//           }
//         }
//       `
//     );
  
//     if (result.errors) {
//       throw result.errors;
//     }
  
//     // Create blog articles pages.
//     const articles = result.data.articles.edges;
  
//     const ArticleTemplate = require.resolve("./src/templates/article.js");
  
//     articles.forEach((article, index) => {
//       createPage({
//         path: `/article/${article.node.slug}`,
//         component: ArticleTemplate,
//         context: {
//           slug: article.node.slug,
//         },
//       });
//     });
//   };

const path = require("path")
  exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions;
    const result = await graphql(
      `
        {
          projects:   allStrapiProjects {
            edges {
              node {
                Description
                Title
                videoLink
                id
                Slug
                internal {
                  type
                }
                cover {
                  publicURL
                }
              }
            }
          }
        }
      `
    );
  
    if (result.errors) {
      throw result.errors;
    }
  
    const projects = result.data.projects.edges;
    projects.forEach((project, index) => {
      createPage({
        path: `/portfolio/${project.node.Slug}`,
        component: path.resolve("./src/templates/project.js"),
        context: {
          Slug: project.node.Slug,
        },
      });
    });

  };