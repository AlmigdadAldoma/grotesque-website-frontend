require("dotenv").config({
  path: `.env`,
});

module.exports = {
  plugins: [
    // {
    //   resolve: 'gatsby-plugin-load-script',
    //   options: {
    //     disable: !process.env.SENTRY_DSN, // When do you want to disable it ?
    //     src: 'https://cdn.jsdelivr.net/npm/circletype@2.3.0/dist/circletype.min.js',
    //     integrity:
    //       'sha384-Nrg+xiw+qRl3grVrxJtWazjeZmUwoSt0FAVsbthlJ5OMpx0G08bqIq3b/v0hPjhB',
    //     crossorigin: 'anonymous',
    //     onLoad: `() => Sentry.init({dsn:"${process.env.SENTRY_DSN}"})`,
    //   },
    // },
    `gatsby-plugin-transition-link`,
    "gatsby-plugin-react-helmet",
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-source-strapi",
      options: {
        apiURL: process.env.DEPLOY_URL 
        ? "https://grotesque-web-cms.herokuapp.com" 
        : "http://localhost:1337",
        contentTypes: ["article", "category", "writer","projects","photographies"],
        singleTypes: [`homepage`, `global`],
        queryLimit: 1000,
      },
    },
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "gatsby-starter-default",
        short_name: "starter",
        start_url: "/",
        background_color: "#663399",
        theme_color: "#663399",
        display: "minimal-ui",
        icon: `src/images/gatsby-icon.png`
      },
    },
    "gatsby-plugin-offline",
    `gatsby-plugin-sass`,
    
    `gatsby-transformer-json`,
{
  resolve: `gatsby-source-filesystem`,
  options: {
    path: `./src/data/`,
  },
},
  ],
};